'use client';
import React, { useState } from 'react';
import styles from './tabBar.module.css';
// type Props = {
//     children?: React.ReactNode;
//     value?: number;
//     right?: number;
//     top?: number;
//     dataAttributes?: DataAttributes;
// };

/**
 * This Component is decorative and won't be read by screenreaders, to make it accessible,
 * set the label to the child element
 *
 * <TabBar value={2}>
 *   <IconButton aria-label="Shopping Cart with 2 items">
 *     <IconShoppingCartFilled />
 *   </IconButton>
 * </TabBar>
 */
const TabBar = () => {
    const [activeTab, setActiveTab] = useState(0);

  const handleTabClick = (index) => {
    setActiveTab(index);
  };

  const renderTab = (index) => {
    const itemColor = Colors.subtitle;

    const tabStyles = [
      styles.tabItem,
      activeTab === index ? styles.activeTab : null,
    ];
    // const iconNames = [
    //   'HomeIcon',
    //   'Benefits',
    //   'Coupons',
    //   'Location',
    //   'Profile',
    // ];
    // const currentIcon = iconNames[index];

    return (
      <div
        className={tabStyles.join(' ')}
        key={index}
        onClick={() => handleTabClick(index)}
      >
        <div className={`tab-content ${styles.tabContent}`}>
          <React.Suspense fallback={null}>
            {/* <Icon icon={currentIcon} active={activeTab === index} /> */}
          </React.Suspense>
          <div style={{ color: itemColor }} className={styles.tabBarText}></div>
        </div>
      </div>
    );
  };

  return <div className={styles.tabBar}>{[0, 1, 2, 3, 4].map(renderTab)}</div>;
};

export default TabBar;
